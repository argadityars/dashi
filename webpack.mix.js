const { mix } = require('laravel-mix');

mix.autoload({
  jquery: ['$', 'jQuery', 'window.jQuery'],
});

mix.sass('src/scss/app.scss', 'dist/css');
mix.js('src/js/app.js', 'dist/js');
mix.options({
  postCss: [require('autoprefixer')],
});
mix.browserSync({
  proxy: 'dashi.test',
  files: ["*.html", "dist/*"],
  notify: false
});
mix.disableNotifications();
mix.setPublicPath(path.normalize('dist'));