## Dashi - Simple Bootstrap 4 Dashboard Template

### Plugins
1. Bootstrap 4
2. jQuery
3. Popper.js
4. simple-line-icons
5. jQuery slimScroll

### How to Install
#### Using git
1. `git clone https://gitlab.com/argadityars/dashi.git`
2. `npm install`
3. Use `index.html` as an example

#### Manually
1. Download `dist` folder
2. Download `index.html` and use it as your starting point